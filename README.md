# README #

PASTA helps instructors assessing the skills of students and provides these students with instant feedback not exclusively based on their programming skills but also on
their ability to design algorithms, understand network protocols, test databases, reason logically, measure complexity, etc.
PASTA Arrabbiata is the open source version of PASTA with a generic interface, docker container support and documentation. 

Documentation is available on the [wiki](https://bitbucket.org/gramoli/arrabbiata/wiki/Home).

### Who do I talk to? ###

  * Vincent Gramoli <vincent.gramoli@sydney.edu.au>
  * Martin McGrane <mmcg5982@uni.sydney.edu.au>